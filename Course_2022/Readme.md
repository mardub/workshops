# Note to participants

This folder contains all the [courses](https://docs.google.com/document/d/1JQ3NlHkY5gQtPhbUqk3GT8CBDwQ6-i3kW2O08sBe_nU/edit#heading=h.k03ezyo20hid) taught by Marie Dubremetz in 2022.
This folder contains:

- [Slides for the NLP course](Basics_of_NLP.pdf) also available [here](https://docs.google.com/presentation/d/1kUxzC1BU0221svF37h-ap_xnRH0_uWmIybB0kTZ0VIY/edit?usp=sharing)
- Downloadable [notebook Webscraping](https://gitlab.com/mardub/workshops/-/blob/master/Course_2022/webscrapingcdhu.ipynb) and [notebook NLP](https://gitlab.com/mardub/workshops/-/raw/master/Course_2022/part-of-speech-tagging-for-swedish.ipynb?inline=false)
- The link to the python notebook.[https://www.kaggle.com/code/pyoupyou/part-of-speech-tagging-for-swedish](https://www.kaggle.com/code/pyoupyou/part-of-speech-tagging-for-swedish)
- In case you cannot run the script on your machine you can go on kaggle.com and load the notebook on it.
[Kaggle link Webscraping](https://www.kaggle.com/code/pyoupyou/webscrapingcdhu) and
[kaggle link NLP](https://www.kaggle.com/code/pyoupyou/part-of-speech-tagging-for-swedish) 

- [Webscraping course](Webscraping.pdf)
- [Webscraping assignment](Assignment_Webscraping.pdf)
- [Registration to webscraping oral exam](https://framadate.org/To9xfyUic9CLNVMJ)

- Module 3 on webscraping:
Additionally to the course material present in this folder you should also install [webscraper.io](https://webscraper.io) and follow their [tutorials](https://webscraper.io/tutorials)

The courses are on site, not distance. If you miss a class, it is your responsibility to work all the course material and discuss thouroughly with your classmates to know what has been instructed for passing the exam.

---

# Note to self (for the teacher):

for this class make sure you have, the flash cards, a pointer, water. 
- write on the blackboard: http://workshops.uppsala.ai
- [NLP notebook on kaggle](https://www.kaggle.com/code/pyoupyou/part-of-speech-tagging-for-swedish)
- [Webscraping notebook on kaggle](https://www.kaggle.com/code/pyoupyou/webscrapingcdhu/edit)
- if questions on API recommand [postman](https://www.getpostman.com)
- run 
```
cd ~/Documents/workshops/Course_2022
. ./course_pack/bin/activate
jupyter notebook
```
before running any python notebook
