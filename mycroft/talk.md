# What is mycroft?
- Personal vocal assistant
	- ex: Alexa, google home
	- open source, free, theoritically installable on any device that supports linux
	- No add, or data selling

# My presentation parts:

	- How to install mycroft
	- demo: what can it do?
	- a bit of code exploration
	- Conclusion

# How to install mycroft:


```yaml
- Show: 
- the video install_SHORT.mp4 (at minute 2:35 pass until installed 
- explain that if you are default user not developping much 
just press yes all the time jump to 3:50m
- Stop at 4 minutes.
```
- Warning there is one step to installing that I skept called “pair your device”.
- The first time you run mycroft go to a your **home.mycroft.ai** account and to click on device + click on pair a device and enter the code.
- Finally we can RUN!

## Demo:
```yaml
- ./start-mycroft.sh restart debug
- “Hey mycroft, can you sing a song?”
- “Hey mycroft what is the weather today”?
- “Hey mycroft tell me about Uppsala”
```
---
# What is behind mycroft?

- Behind mycroft there are some small program that are called “skills”
A skill is a set of python programs/components that you can add and remove to your mycroft to make it more adapted to your needs.
- Lets take a simple user case. I am playing a game and I want to use mycroft to roll a dice for me. Ie. Tell me a random number between 1 and 6. Let’s try the vocal command:
```yaml
	- “hey mycroft install Roll a dice”
	- “Dice”
	- “hej mycroft can you roll a dice”
```
- As we can see behind the scene mycroft is doing a git “pull” and adding a new folder full of python script to my skill folder.
- Hey mycroft remove skill ‘dice’

# These skills can be developped by you.
For instance I needed to turn on and off my projector with my voice because the button is not accessible so I made the following program and loaded it on git:
```yaml
- Go to “https://github.com/mardub1635/projector-switch-skill”
- show them the video of what your skill could do.
```
- In a skill there is mainly 2 elements:
1. The locale folder containing :
- the intents files, these are text files that define the "trigger" of the skill.
- the dialog files, text files that define what mycroft will tell you.
``` python 
___init__.py 
```
2. The main python program.
-  This will define what mycroft must execute when this skill is triggered. In my case it was a ssh command followed by some obscure command line to order the projector to switch on and off. There is relatively good documentation and you can copy paste other skills that look a bit like yours and just adapt them.

Conclusion:
- Mycroft is a vocal assistant
The drawbacks:
- Linux friendly (Ubuntu/debian). Hard to run on other devices: need virtual machine, docker and all it implies in terms of interaction problems (setting up the micro+ sound correctly, having to ssh on your own computer if you want to run any command etc.)
- Thus can be a pain when you start to do maintainance (problem of upgrade intrusive, had to stop).

The positive:
- New, modern, our future is there.
- Safe and respectful of privacy thanks to the free Open-source community
- Lot of fun, when running skill and giving feedback the community can be really helpful
- Could be good for prototyping a project, proof of concept.
- It is reusing standard technologies that a lot of developpers know already such as git unlike alexa where you must learn a whole new platform and tool closed by amazon and with an amazon account. Thus at the same time you developp you deepen development skills more universal.
