The instruction are in the slides of the course. Look at the begining of the slides. https://gitlab.com/mardub/workshops/-/blob/master/NLP/Basics_of_NLP.pdf


Name: Sven Exempelson

Field: Department of anthropology, sven.exempelson@anthopology.uu.se

# Evaluation: working on NLP notions in your field
a. I chose:
Mermaids are birds. Text Mining N.F.S. Grundtvig’s Bestiary
 Katrine Frøkjær Baunvig, Kristoffer Laigaard Nielbo
in  DHNB 2022 Conference : Book of Abstracts. 
http://uu.diva-portal.org/smash/get/diva2:1650296/FULLTEXT01.pdf

This abstract is in the domain of anthropology. In this paper the authors use NLP in order to explore the use a metaphora.

b. The corpus is in Danish.

c. The size they report of their corpus is a bit vague. They mention 1068 publication but we do not know how many words that would represent.

d. They use the following methods:
- word embedding
- tokenization
- lemmatization


The tools they use to implement are:
- the programming language python
...
We do not know what package they used for the tokenization and lemmatisation the reading of the paper itself or maybe an exploration of their personal implementation if open source could give us a hint.

e. I added the information on the spreadsheet: https://cloud.fripost.org/s/2p5aQqctdab4R8Y 
