# About
This is a workshop about webscraping given to different units of Uppsala university.

Be aware that the slides are slightly changed over time to update and be adapted to the audience.

Gitlab has a "History" button that can give you access to previous versions of slides.

You can always access Marie's course and workshops (incl. this one!) if you write in your browser:
[http://workshops.uppsala.ai](http://workshops.uppsala.ai) and go to the appropriate folder.

# For techies

Direct access to the python notebook can be found on Kaggle [https://www.kaggle.com/code/pyoupyou/webscrapingcdhu](https://www.kaggle.com/code/pyoupyou/webscrapingcdhu) but you can always download it directly on your machine (file *.ipynb) if you have anaconda installed.

